# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 02:01:29 2019

@author: rkm977
"""


import numpy as np
import matplotlib.pyplot as plt
import sys


def sample_average(test_bed_size, N, arms, epsilon):
    
    total_avg_reward = np.zeros(N)
    optimal_counter = np.zeros(N)
    
    for problem in range(test_bed_size):
        Q_initial_val = 0
        Q_value = Q_initial_val * np.ones(arms)
        Q_optimal = np.zeros(arms)
        
        counter = np.zeros(arms)
        sum_reward = np.zeros(arms)
        avg_reward = np.zeros(N)
        
        for iter in range(N):
            if (np.random.binomial(1, epsilon)):
                action = np.random.randint(0, arms-1)
            else:
                action = np.argmax(Q_value)
            reward = np.random.normal(Q_optimal[action], 1.0)
            
            if counter[action] == 0:
                Q_value[action] = 0
            else:
                Q_value[action] = sum_reward[action] / counter[action]
                
            optimal_action = np.argmax(Q_optimal)
                
            if action == optimal_action:
                optimal_counter[iter] = optimal_counter[iter] + 1
            
            counter[action] = counter[action] + 1
            sum_reward[action] = sum_reward[action] + reward
            Q_optimal = Q_optimal + np.random.normal(0, .01, arms)
            
            avg_reward[iter] = reward
            
        total_avg_reward = (problem * total_avg_reward + avg_reward) / (problem + 1)
        
    optimal_counter = optimal_counter/test_bed_size
    return total_avg_reward, optimal_counter

def action_value_step(test_bed_size, N, arms, epsilon, alpha):
    
    total_avg_reward = np.zeros(N)
    optimal_counter = np.zeros(N)
    
    
    for problem in range(test_bed_size):
        Q_initial_val = 0
        Q_value = Q_initial_val * np.ones(arms)
        Q_optimal = np.zeros(arms)
        
        avg_reward = np.zeros(N)
        
        for iter in range(N):
            if (np.random.binomial(1, epsilon)):
                action = np.random.randint(0, arms-1)
            else:
                action = np.argmax(Q_value)

            reward = np.random.normal(Q_optimal[action], 1.0)
            optimal_action = np.argmax(Q_optimal)
            
            if action == optimal_action:
                optimal_counter[iter] = optimal_counter[iter] + 1
                
                
            Q_value[action] = Q_value[action] + alpha * (reward - Q_value[action])
            avg_reward[iter] = reward
            
            Q_optimal = Q_optimal + np.random.normal(0, .01, arms)
            
        total_avg_reward = (problem * total_avg_reward + avg_reward) / (problem + 1)
        
    optimal_counter = optimal_counter/test_bed_size
    return total_avg_reward, optimal_counter



def main():
    
    test_bed_size = 300
    N = 10000
    arms = 10
    
    fname = sys.argv[1]
    
    reward1, optimal_action1 = sample_average(test_bed_size, N, arms, .1)
    reward2, optimal_action2 = action_value_step(test_bed_size, N, arms, .1, .1)
    
    
    Out = np.zeros((4,N))
    Out[0,:] = reward1
    Out[1,:] = optimal_action1
    Out[2,:] = reward2
    Out[3,:] = optimal_action2
    
    np.savetxt(fname, Out)
    
    # plt.figure()
    # plt.plot(reward1)
    # plt.plot(reward2)
    # plt.show()
    
    # plt.figure
    # plt.plot(optimal_action1)
    # plt.plot(optimal_action2)
    # plt.show()
  
if __name__ == "__main__":
    main()