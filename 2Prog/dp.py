from typing import Tuple

import numpy as np
from env import EnvWithModel
from policy import Policy


class deterministic_policy(Policy):
    def __init__(self, nA, nS, p=None):
        self.p = np.zeros((nS, nA))
        for s in range(nS):
            self.p[s, :] = p[s, :] if p is not None and sum(
                p[s, :]) == 1 else np.array([1/nA]*nA)

    def action_prob(self, state, action=None):
        return self.p[state, action]

    def action(self, state):
        print("p = ", self.p)
        return np.random.choice(len(self.p[state, :]), p=self.p[state, :])


def value_prediction(env: EnvWithModel, pi: Policy, initV: np.array, theta: float) -> Tuple[np.array, np.array]:
    """
    inp:
        env: environment with model information, i.e. you know transition dynamics and reward function
        pi: policy
        initV: initial V(s); numpy array shape of [nS,]
        theta: exit criteria
    return:
        V: $v_\pi$ function; numpy array shape of [nS]
        Q: $q_\pi$ function; numpy array shape of [nS,nA]
    """

    V = initV
    Q = np.zeros((env.spec.nS, env.spec.nA))

    err = 100

    while err > theta:
        err = 0
        for s in range(env.spec.nS):
            v = V[s]
            Vtemp = 0
            for a in range(env.spec.nA):
                for sn in range(env.spec.nS):
                    Vtemp = Vtemp + \
                        pi.action_prob(
                            s, a)*(env.TD[s, a, sn]*(env.R[s, a, sn] + env.spec.gamma*V[sn]))
            V[s] = Vtemp
            err = max(err, abs(v - V[s]))

    for s in range(env.spec.nS):
        for a in range(env.spec.nA):
            Vtemp = 0
            for sn in range(env.spec.nS):
                Vtemp = Vtemp + env.TD[s, a, sn] * \
                    (env.R[s, a, sn] + env.spec.gamma*V[sn])

            Q[s, a] = Vtemp

    return V, Q


def value_iteration(env: EnvWithModel, initV: np.array, theta: float) -> Tuple[np.array, Policy]:
    """
    inp:
        env: environment with model information, i.e. you know transition dynamics and reward function
        initV: initial V(s); numpy array shape of [nS,]
        theta: exit criteria
    return:
        value: optimal value function; numpy array shape of [nS]
        policy: optimal deterministic policy; instance of Policy class
    """
    V = initV
    err = 100
    p = np.zeros((env.spec.nS, env.spec.nA))
    while err > theta:
        err = 0
        for s in range(env.spec.nS):
            v = V[s]
            Vtemp = np.zeros(env.spec.nA)
            for a in range(env.spec.nA):
                for sn in range(env.spec.nS):
                    Vtemp[a] = Vtemp[a] + env.TD[s, a, sn] * \
                        (env.R[s, a, sn] + env.spec.gamma*V[sn])
            V[s] = max(Vtemp[:])
            p[s, :] = np.zeros(env.spec.nA)
            p[s, np.argmax(Vtemp)] = 1
            err = max(err, abs(v - V[s]))

    pi = deterministic_policy(env.spec.nA, env.spec.nS, p)

    return V, pi
