class Person:
  def __init__(self, fname, lname):
    self.firstname = fname
    self.lastname = lname

  def printname(self):
    print(self.firstname, self.lastname)

#Use the Person class to create an object, and then execute the printname method:

x = Person("John", "Doe")
x.printname()

class Student(Person):
  pass


class Student1(Person):
  def __init__(self, fname, lname):
    Person.__init__(self, fname, lname)
    

class Student2(Person):
  def __init__(self, fname, lname):
    super().__init__(fname, lname)
    self.graduationyear = 2019
    
    
x = Student("Mike", "Olsen")
x.printname()

y = Student1("Mike", "Olsen")
y.printname()

z = Student2("Rajesh", "Mishra")
z.printname()