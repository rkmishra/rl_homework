from typing import Iterable, Tuple

import numpy as np
from env import EnvSpec
from policy import Policy


class modified_policy(Policy):
    def __init__(self, nA, nS, p=None):
        self.p = np.zeros((nS, nA))
        for s in range(nS):
            self.p[s, :] = p[s, :] if p is not None and sum(
                p[s, :]) == 1 else np.array([1/nA]*nA)

    def action_prob(self, state, action=None):
        return self.p[state, action]

    def action(self, state):
        return np.random.choice(len(self.p[state, :]), p=self.p[state, :])


def on_policy_n_step_td(
    env_spec: EnvSpec,
    trajs: Iterable[Iterable[Tuple[int, int, int, int]]],
    n: int,
    alpha: float,
    initV: np.array
) -> Tuple[np.array]:
    """
    input:
        env_spec: environment spec
        trajs: N trajectories generated using
            list in which each element is a tuple representing (s_t,a_t,r_{t+1},s_{t+1})
        n: how many steps?
        alpha: learning rate
        initV: initial V values; np array shape of [nS]
    ret:
        V: $v_pi$ function; numpy array shape of [nS]
    """
    V = initV

    for e in range(len(trajs)):

        T = len(trajs[e])
        for tau in range(T):

            G = 0

            for j in range(min(n, T - tau)):
                G = G + (env_spec.gamma**j)*trajs[e][tau + j][2]
            S_tau = trajs[e][tau][0]
            if tau + n < T:
                Sn_tau = trajs[e][tau + n][0]
                G = G + (env_spec.gamma**n)*V[Sn_tau]
            V[S_tau] = V[S_tau] + alpha*(G - V[S_tau])

    return V


def off_policy_n_step_sarsa(
    env_spec: EnvSpec,
    trajs: Iterable[Iterable[Tuple[int, int, int, int]]],
    bpi: Policy,
    n: int,
    alpha: float,
    initQ: np.array
) -> Tuple[np.array, Policy]:
    """
    input:
        env_spec: environment spec
        trajs: N trajectories generated using
            list in which each element is a tuple representing (s_t,a_t,r_{t+1},s_{t+1})
        bpi: behavior policy used to generate trajectories
        n: how many steps?
        alpha: learning rate
        initQ: initial Q values; np array shape of [nS,nA]
    ret:
        Q: $q_star$ function; numpy array shape of [nS,nA]
        policy: $pi_star$; instance of policy class
    """

    #####################
    # TODO: Implement Off Policy n-Step SARSA algorithm
    # sampling (Hint: Sutton Book p. 149)
    #####################

    Q = initQ
    p = np.zeros((env_spec.nS, env_spec.nA))
    pi = modified_policy(env_spec.nA, env_spec.nS)
    for e in range(len(trajs)):
        T = len(trajs[e])
        for tau in range(T):
            G = 0
            rho = 1
            for j in range(min(n, T - tau)):
                G = G + (env_spec.gamma**j)*trajs[e][tau + j][2]
            for j in range(min(n, T - tau - 1)):
                s = trajs[e][tau + j][0]
                a = trajs[e][tau + j][1]
                rho = rho*(pi.action_prob(s, a)/bpi.action_prob(s, a))
            S_tau = trajs[e][tau][0]
            A_tau = trajs[e][tau][1]
            if tau + n < T:
                Sn_tau = trajs[e][tau + n][0]
                An_tau = trajs[e][tau + n][1]
                G = G + (env_spec.gamma**n)*Q[Sn_tau, An_tau]
            Q[S_tau, A_tau] = Q[S_tau, A_tau] + alpha*rho*(G - Q[S_tau, A_tau])
            p[S_tau, :] = np.zeros(env_spec.nA)
            p[S_tau, np.argmax(Q[S_tau, :])] = 1

    pi = modified_policy(env_spec.nA, env_spec.nS, p)
    return Q, pi
